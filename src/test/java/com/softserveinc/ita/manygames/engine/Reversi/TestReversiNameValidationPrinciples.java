package com.softserveinc.ita.manygames.engine.Reversi;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Igor Khlaponin
 */
public class TestReversiNameValidationPrinciples {

    private Reversi game;

    @Before
    public void setUp() {
        game = new Reversi();
    }

    @Test
    public void checkIfPlayerNameIsNull() {
        assertFalse(game.validatePlayerName(null));
    }

    @Test
    public void checkIfPlayerNameIsEmptyString() {
        assertFalse(game.validatePlayerName(""));
    }

    @Test
    public void checkIfPlayerNameIsSomeString() {
        assertTrue(game.validatePlayerName("someString"));
    }
}
