package com.softserveinc.ita.manygames.engine.Reversi;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Igor Khlaponin
 */
public class TestReversiTurnSyntaxValidationPrinciples {

    private Reversi game;

    @Before
    public void setUpGame(){
        game = new Reversi();
    }

    @Test
    public void checkIfPlayerTurnContainsNotTwoSymbols(){
        boolean actualValue = game.validateTurnSyntax("sd3");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfPlayersTurnIsEmpty(){
        boolean actualValue = game.validateTurnSyntax("");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfPlayersTurnIsNull(){

        boolean actualValue = game.validateTurnSyntax(null);
        assertFalse(actualValue);
    }

    @Test
    public void checkIfSetRightTurnSyntaxWithLowerCaseLetter(){

        boolean actualValue = game.validateTurnSyntax("e4");
        assertTrue(actualValue);
    }

    @Test
    public void checkIfSetRightTurnSyntaxWithCapitalLetter(){

        boolean actualValue = game.validateTurnSyntax("E4");
        assertTrue(actualValue);
    }

    @Test
    public void checkIfSetWrongCapitalLetter(){

        boolean actualValue = game.validateTurnSyntax("Z4");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfSetWrongLowerCaseLetter(){

        boolean actualValue = game.validateTurnSyntax("x4");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfPlayersTurnContainsWrongCharacter(){

        boolean actualValue = game.validateTurnSyntax("*3");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfPlayersTurnDoesNotContainDigit(){

        boolean actualValue = game.validateTurnSyntax("ax");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfPlayersTurnDoesNotContainLetter(){

        boolean actualValue = game.validateTurnSyntax("11");
        assertFalse(actualValue);
    }

    @Test
    public void checkIfPlayersTurnTypesInWrongOrder(){

        boolean actualValue = game.validateTurnSyntax("4e");
        assertFalse(actualValue);
    }


}
