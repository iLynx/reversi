package com.softserveinc.ita.manygames.engine.Reversi;

import com.softserveinc.ita.manygames.engine.GameState;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @author Igor Khlaponin
 */
public class TestBoard {

    private Board board;

    @Before
    public void setUp(){
        board = new Board();
    }

    @Test
    public void defaultFieldSizeEqualsTo8(){
        board = new Board();
        int fieldSize = board.getBoardSize();
        assertEquals(8, fieldSize);
    }

    @Test
    public void checkIfPawnSettingInCurrentCell(){

        board.putPawn("f4", GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[4][5]);
    }

    @Test
    public void checkColorChangingIfBlackPawnsAreLayingBetweenSetWhitePawnAndLastRightWhitePawn(){

        board.transformField(4, 5, Color.WHITE);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[4][4]);
    }

    @Test
    public void checkColorChangingIfBlackPawnsAreLayingBetweenWhitePawnAndFirstLeftWhitePawn(){

        board.transformField(2, 3, Color.WHITE);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[3][3]);
    }

    @Test
    public void checkColorChangingIfBlackPawnsAreLayingBetweenWhitePawnAndUpperWhitePawn(){

        board.transformField(3, 2, Color.WHITE);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[3][3]);
    }

    @Test
    public void checkColorChangingIfBlackPawnsAreLayingBetweenWhitePawnAndLowerWhitePawn(){

        board.transformField(4, 5, Color.WHITE);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[4][4]);
    }

    @Test
    public void checkColorChangingIfWeSetPawnUpAndLeftToTheCurrentPawn(){
        board.putPawn("e3", GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        board.putPawn("f5", GameState.WAIT_FOR_SECOND_PLAYER_TURN);

        board.transformField(6, 2, Color.WHITE);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[3][5]);
    }

    @Test
    public void checkColorChangingIfWeSetPawnUpAndRightToTheCurrentPawn(){
        board.putPawn("e3", GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        board.putPawn("f5", GameState.WAIT_FOR_SECOND_PLAYER_TURN);

        board.transformField(6, 2, Color.WHITE);
        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[3][5]);
    }

    @Test
    public void checkColorChangingIfWeSetPawnDownAndLeftToTheCurrentPawn(){
        board.putPawn("f4",GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        board.putPawn("f5",GameState.WAIT_FOR_SECOND_PLAYER_TURN);
        board.putPawn("f6",GameState.WAIT_FOR_FIRST_PLAYER_TURN);

        int[][] field = board.getField();

        assertEquals(Color.WHITE, field[3][4]);
    }

    @Test
    public void checkColorChangingIfWeSetPawnDownAndRightToTheCurrentPawn(){
        board.putPawn("f4",GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        board.putPawn("f3",GameState.WAIT_FOR_SECOND_PLAYER_TURN);

        int[][] field = board.getField();

        assertEquals(Color.BLACK, field[4][4]);
    }

    @Test
    public void checkIfTheGameHaveMoreTurns(){

        assertTrue(board.hasMoreTurns());
    }


}
