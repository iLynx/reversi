package com.softserveinc.ita.manygames.engine.Reversi;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Igor Khlaponin
 */
public class Util {

    public static Map<Integer, Integer> turnParser(String playerTurn){ //i.e. e4; g3; a4 etc.
        Map<Integer, Integer> turn = new HashMap<>();
        char charX = playerTurn.charAt(0); //letter
        char charY = playerTurn.charAt(playerTurn.length()-1); //digit from 1 to 8
        int x = -1, y = -1;

        //x parsing
        //65-72; 97-104 letters from a to h and from A to H
        if(charX >= 65 && charX <= 72){
            x = getArrayIndexByCharCode(charX, 65, 72);
        } else if(charX >= 97 && charX <= 104){
            x = getArrayIndexByCharCode(charX, 97, 104);
        }

        //y parsing
        //49-56 - digits from 1 to 8
        int temp = 7;
        for(int i = 49; i <= 56; i++){
            if(i == charY) {
                y = temp;
                break;
            } else {
                temp--;
            }
        }

        turn.put(x,y);

        return turn;
    }
    private static int getArrayIndexByCharCode(char character, int beginRange, int endRange){
        int index = -1;

        int temp = 0;
        for(int i = beginRange; i <= endRange; i++){
            if(i == character){
                index = temp;
                break;
            }else {
                temp++;
            }
        }

        return index;
    }
}
