package com.softserveinc.ita.manygames.engine.Reversi;

import com.softserveinc.ita.manygames.engine.GameState;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Igor Khlaponin
 */
public class App {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Reversi game = new Reversi();

        while (game.getCurrentGameState() == GameState.WAIT_FOR_FIRST_PLAYER_NAME) {
            System.out.print("Enter first player name: ");
            game.setFirstPlayer(reader.readLine());
        }

        while (game.getCurrentGameState() == GameState.WAIT_FOR_SECOND_PLAYER_NAME) {
            System.out.print("Enter second player name: ");
            game.setSecondPlayer(reader.readLine());
        }
        game.getGameBoard().printBoard();

        while (!game.isFinished()) {
        	System.out.println(game.getCurrentPlayer() + "'s turn");
            game.makeTurn(game.getCurrentPlayer(),reader.readLine());
            game.getGameBoard().printBoard();
        }
        System.out.println("Winner is " + game.getTheWinner());

        reader.close();
    }
    
    
}
